using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace Protocol
{
    public class MsgWrapper
    {
        public string MsgType { get; set; }

        public JToken Data { get; set; }

        public string GameId { get; set; }

        public uint GameTick { get; set; }

        public MsgWrapper(string msgType, JToken data)
        {
            this.MsgType = msgType;
            this.Data = data;
        }

        public T DataAs<T>()
        {
            return Data.ToObject<T>();
        }
    }

    public abstract class SendMsg
    {
        public string ToJson()
        {
            return JsonConvert.SerializeObject(
                new MsgWrapper(MsgType(), MsgData()),
                Formatting.None,
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    DefaultValueHandling = DefaultValueHandling.Ignore
                });
        }

        protected virtual JToken MsgData()
        {
            var serializer = JsonSerializer.Create(new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
            return JToken.FromObject(this, serializer);
        }

        abstract public string MsgType();
    }

    public class Join : SendMsg
    {
        public string Name { get; set; }

        public string Key { get; set; }

        public string Color { get; set; }

        public Join(string name, string key)
        {
            this.Name = name;
            this.Key = key;
            this.Color = "red";
        }

        public override string MsgType()
        {
            return "join";
        }
    }

    public class BotId
    {
        public string Name { get; set; }

        public string Key { get; set; }

        public BotId(string name, string key)
        {
            Name = name;
            Key = key;
        }
    }

    public class JoinRace : SendMsg
    {
        public BotId BotId { get; set; }

        public string TrackName { get; set; }

        public string Password { get; set; }

        public uint CarCount { get; set; }

        public JoinRace(
            BotId botId,
            string trackName = "keimola",
            string password = "",
            uint carCount = 1)
        {
            BotId = botId;
            TrackName = trackName;
            Password = password;
            CarCount = carCount;
        }

        public override string MsgType()
        {
            return "joinRace";
        }
    }

    public class CreateRace : JoinRace
    {
        public CreateRace(
            BotId botId,
            string trackName = "keimola",
            string password = "",
            uint carCount = 1)
            : base(
                botId,
                trackName,
                password,
                carCount
            )
        {
        }

        public override string MsgType()
        {
            return "createRace";
        }
    }

    public class Ping : SendMsg
    {
        public override string MsgType()
        {
            return "ping";
        }
    }

    public class Throttle : SendMsg
    {
        public double Value { get; set; }

        public Throttle(double value)
        {
            this.Value = value;
        }

        protected override JToken MsgData()
        {
            return JToken.FromObject(Value);
        }

        public override string MsgType()
        {
            return "throttle";
        }
    }

    public enum SwitchDirection
    {
        Left,
        Right
    }

    public class SwitchLane : SendMsg
    {
        public SwitchDirection Direction { get; set; }

        public SwitchLane(SwitchDirection dir)
        {
            this.Direction = dir;
        }

        protected override JToken MsgData()
        {
            switch (Direction)
            {
                case SwitchDirection.Left:
                    return JToken.FromObject("Left");
                default:
                    return JToken.FromObject("Right");
            }
        }

        public override string MsgType()
        {
            return "switchLane";
        }
    }
}
