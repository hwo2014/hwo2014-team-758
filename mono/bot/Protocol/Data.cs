using System;
using System.Collections.Generic;

namespace Protocol
{
    public class GameInit
    {
        public Race Race { get; set; }
    }

    public class Race
    {
        public TrackInfo Track { get; set; }

        public List<Car> Cars { get; set; }

        public RaceSession RaceSession { get; set; }
    }

    public class RaceSession
    {
        public uint Laps { get; set; }

        public uint MaxLapTimeMs { get; set; }

        public bool QuickRace { get; set; }
    }

    public class TrackInfo
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public List<TrackPiece> Pieces { get; set; }

        public List<Lane> Lanes { get; set; }

        public int FindFirstFrom(uint pieceIndex, Func<TrackPiece, bool> f)
        {
            for (uint i = (uint)(((int)pieceIndex + 1) % Pieces.Count); i != pieceIndex; i = (uint)((int)(i + 1) % Pieces.Count))
            {
                var piece = Pieces[(int)i];
                if (f(piece))
                {
                    return (int)i;
                }
            }
            return -1;
        }
    }

    public class TrackPiece
    {
        public double Length { get; set; }

        public bool Switch { get; set; }

        public double Radius { get; set; }

        public double Angle { get; set; }

        public bool IsTurn
        {
            get { return Length <= 0.0; }
        }

        public double LengthForLane(Lane lane)
        {
            if (!IsTurn)
            {
                return Length;
            }
            return (Radius + Math.Sign(-Angle) * lane.DistanceFromCenter) * Math.PI * Math.Abs(Angle) / 180;
        }
    }

    public class Lane
    {
        public uint Index { get; set; }

        public double DistanceFromCenter { get; set; }
    }

    public class Car
    {
        public CarId Id { get; set; }

        public CarDimensions Dimensions { get; set; }
    }

    public class CarDimensions
    {
        public double Length { get; set; }

        public double Width { get; set; }

        public double GuideFlagPosition { get; set; }
    }

    public class CarId
    {
        public string Name { get; set; }

        public string Color { get; set; }

        public CarId(string name, string color)
        {
            Name = name;
            Color = color;
        }

        public override bool Equals(Object obj)
        {
            var carId = obj as CarId;
            if (carId == null)
            {
                return false;
            }
            return Name.Equals(carId.Name) && Color.Equals(carId.Color);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ Color.GetHashCode();
        }
    }

    public class CarPositions
    {
        private readonly Dictionary<CarId, CarPosition> posMap =
            new Dictionary<CarId, CarPosition>();

        public List<CarPosition> Positions { get; private set; }

        private readonly CarId myCar;

        public CarPositions(CarId myCar, List<CarPosition> carPositions)
        {
            this.myCar = myCar;
            Positions = carPositions;
            foreach (var cp in carPositions)
            {
                posMap[cp.Id] = cp;
            }
        }

        public CarPosition Position
        {
            get { return PositionForId(myCar); }
        }

        public CarPosition PositionForId(CarId id)
        {
            return posMap[id];
        }
    }

    public class CarPosition
    {
        public class PiecePos
        {
            public uint PieceIndex { get; set; }

            public double InPieceDistance { get; set; }

            public int Lap { get; set; }

            public class LaneSwitch
            {
                public uint StartLaneIndex { get; set; }

                public uint EndLaneIndex { get; set; }
            }

            public LaneSwitch Lane { get; set; }
        }

        public CarId Id { get; set; }

        public double Angle { get; set; }

        public PiecePos PiecePosition { get; set; }
    }

    public class LapFinished
    {
        public CarId Car { get; set; }

        public class LapTimeData
        {
            public uint Lap { get; set; }

            public uint Ticks { get; set; }

            public uint Millis { get; set; }
        }

        public LapTimeData LapTime { get; set; }

        public class RaceTimeData
        {
            public uint Laps { get; set; }

            public uint Ticks { get; set; }

            public uint Millis { get; set; }
        }

        public RaceTimeData RaceTime { get; set; }

        public class RankingData
        {
            public uint Overall { get; set; }

            public uint FastestLap { get; set; }
        }

        public RankingData Ranking { get; set; }
    }

    public class Dnf
    {
        public CarId Car { get; set; }

        public string Reason { get; set; }
    }
}
