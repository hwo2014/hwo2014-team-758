using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;

using Protocol;
using Util;

public class Constants
{
    public int ThrottleDelay { get; set; }

    public double AccFullThrottle { get; set; }

    public double ConstantAccRatio { get; set; }

    public double Friction { get; set; }

    public double MaxSpeedOne { get; set; }

    public double ThrottleForSpeed(double targetSpeed)
    {
        return targetSpeed / (MaxSpeedOne * 10);
    }

    public double MaxDriftAngle { get; set; }
}

public class Bot
{
    public static void Main(string[] args)
    {
        string host = args[0];
        int port = int.Parse(args[1]);
        string botName = Environment.GetEnvironmentVariable("BOT_NAME") ?? args[2];
        string botKey = args[3];

        Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        using (var client = new TcpClient(host, port))
        {
            NetworkStream stream = client.GetStream();
            var reader = new StreamReader(stream);
            var writer = new StreamWriter(stream);
            writer.AutoFlush = true;

            SendMsg join = new Join(botName, botKey);
            if (Environment.GetEnvironmentVariable("TRACK_NAME") != null)
            {
                var racePass = Environment.GetEnvironmentVariable("RACE_PASS") ?? "";
                uint numCars = 1;
                if (Environment.GetEnvironmentVariable("NUM_CARS") != null)
                {
                    uint.TryParse(Environment.GetEnvironmentVariable("NUM_CARS"), out numCars);
                }
                if (Environment.GetEnvironmentVariable("CREATE_RACE") != null)
                {
                    join = new CreateRace(
                        new BotId(botName, botKey),
                        Environment.GetEnvironmentVariable("TRACK_NAME"),
                        racePass,
                        numCars
                    );
                }
                else
                {
                    join = new JoinRace(
                        new BotId(botName, botKey),
                        Environment.GetEnvironmentVariable("TRACK_NAME"),
                        racePass,
                        numCars
                    );
                }
            }

            new Bot(reader, writer, join);
        }
    }

    private readonly StreamWriter writer;

    Bot(StreamReader reader, StreamWriter writer, SendMsg join)
    {
        this.writer = writer;
        string line;

        send(join);

        var constants = new Constants();
        constants.ThrottleDelay = 1;
        constants.AccFullThrottle = 0.2;
        constants.ConstantAccRatio = 0.98;
        constants.Friction = 0.98;
        constants.MaxSpeedOne = 1.0;
        constants.MaxDriftAngle = 60.0;

        CarPosition prevPos = null;
        GameInit game = null;
        CarId carId = null;
        double prevVel = 0.0;
        double prevAcc = 0.0;
		List<object> test = new List<object>();

        while ((line = reader.ReadLine()) != null)
        {
            MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line, new JsonSerializerSettings
                {
                    MissingMemberHandling = MissingMemberHandling.Ignore
                });
            switch (msg.MsgType)
            {
                case "carPositions":
                    var carPositions = new CarPositions(carId, msg.DataAs<List<CarPosition>>());
                    double currVel = 0.0;
                    double currAcc = 0.0;

                    double currThrottle = 0.7;
                    if (msg.GameTick > 0)
                    {
                        currVel = Distance.Between(game.Race.Track, prevPos, carPositions.Position);
                        currAcc = currVel - prevVel;
                        //Console.Write(msg.GameTick + " | ");
                        //Console.Write("Piece: " + carPositions.Position.PiecePosition.PieceIndex);
                        //Console.Write(", Angle: " + game.Race.Track.Pieces[(int)carPositions.Position.PiecePosition.PieceIndex].Angle);
                        //Console.Write(", Radius: " + game.Race.Track.Pieces[(int)carPositions.Position.PiecePosition.PieceIndex].Radius);
                        //Console.Write(game.Race.Track.Pieces[(int)carPositions.Position.PiecePosition.PieceIndex].IsTurn ? ", Turn" : ", Straight");
                        //Console.Write(", InPieceDistance: " + carPositions.Position.PiecePosition.InPieceDistance);
                        //Console.Write(", Acc: " + currAcc);
                        //Console.WriteLine(", Diff: " + currVel);
                        //Console.WriteLine("Slip angle: " + carPositions.Position.Angle);
                        //Console.WriteLine("Slip angle ratio: " + carPositions.Position.Angle / prevPos.Angle);

                        const double turnVel = 9.0;
                        var targetSpeed = 0.0;
                        var currentPiece = game.Race.Track.Pieces[(int)carPositions.Position.PiecePosition.PieceIndex];
                        if (currentPiece.IsTurn)
                        {
                            targetSpeed = turnVel;
                        }
                        else
                        {
                            targetSpeed = 10.0;
                        }
                        var turnIndex = game.Race.Track.FindFirstFrom(carPositions.Position.PiecePosition.PieceIndex, t => t.IsTurn);
                        var distToTurn = Distance.ToPiece(
                                             game.Race.Track,
                                             carPositions.Position,
                                             (uint)turnIndex,
                                             new Dictionary<uint, SwitchDirection>());
                        //Console.WriteLine("Distance to turn: " + distToTurn);
                        if (currVel < turnVel)
                        {
                            //noop
                        }
                        else
                        {
                            var ticksToTargeSpeed = Math.Log(turnVel / currVel) / Math.Log(constants.Friction);
                            var distance = 0.0;
                            var loopVel = currVel;
                            for (int i = 1; i <= ticksToTargeSpeed; i++)
                            {
                                loopVel = loopVel * constants.Friction;
                                distance += loopVel;
                            }
                            //Console.WriteLine("Ticks to target speed: " + ticksToTargeSpeed);
                            //Console.WriteLine("Distance to target speed:" + distance);
                            if (distance >= distToTurn)
                            {
                                targetSpeed = 0.0;
                            }
                        }
                        var targetThrottle = constants.ThrottleForSpeed(targetSpeed);
                        //Console.WriteLine("Throttle: " + targetThrottle);
                        send(new Throttle(targetThrottle));
                        //send(new Throttle(0.79));

						test.Add(msg.GameTick); // game tick
						test.Add(carPositions.Position.PiecePosition.PieceIndex); // piece index
						test.Add(game.Race.Track.Pieces[(int)carPositions.Position.PiecePosition.PieceIndex].Angle); // piece angle
						test.Add(game.Race.Track.Pieces[(int)carPositions.Position.PiecePosition.PieceIndex].Radius); // piece radius
						test.Add(carPositions.Position.PiecePosition.InPieceDistance); // inPieceDistance
						test.Add(currAcc); // curr Acceleration
						test.Add(currVel); // curr Velocity
						test.Add(carPositions.Position.Angle); // currSlip Angle
						test.Add(targetThrottle); // throttle
						csvOutput(test);
						test.Clear();
                    }
                    //if (msg.GameTick == 1)
                    //{
                        //if (carPositions.Position.PiecePosition.Lane.StartLaneIndex == 0)
                        //{
                            //send(new SwitchLane(SwitchDirection.Right));
                        //}
                        //else
                        //{
                            //send(new SwitchLane(SwitchDirection.Left));
                        //}
                    //}
                    //else
                    //{
                        //send(new Throttle(0.655));
                    //}
                    //send(new Throttle(msg.GameTick / 10.0));
                    //Console.WriteLine("Friction: " + (currVel / prevVel));
                    //Console.WriteLine("Accel ratio: " + (currAcc / prevAcc));

                    prevPos = carPositions.Position;
                    prevVel = currVel;
                    prevAcc = currAcc;
                    break;
                case "lapFinished":
                    var lapFinished = msg.DataAs<LapFinished>();
                    Console.WriteLine(String.Format("Car '{0}' finished lap {1} on place {2} on tick {3}",
                            lapFinished.Car.Name,
                            lapFinished.LapTime.Lap,
                            lapFinished.Ranking.Overall,
                            msg.GameTick));
                    break;
                case "dnf":
                    var dnf = msg.DataAs<Dnf>();
                    Console.WriteLine(String.Format("Car '{0}' did not finish with reason '{1}' on tick {2}",
                            dnf.Car.Name,
                            dnf.Reason,
                            msg.GameTick));
                    break;
                case "finish":
                    var finishCarId = msg.DataAs<CarId>();
                    Console.WriteLine(String.Format("Car '{0}' finished on tick '{1}'",
                            finishCarId.Name,
                            msg.GameTick));
                    break;
                case "join":
                    Console.WriteLine("Joined");
                    send(new Ping());
                    break;
                case "gameInit":
                    Console.WriteLine("Race init");
                    game = msg.DataAs<GameInit>();
                    Console.WriteLine("Track name: " + game.Race.Track.Name);
                    Console.WriteLine(String.Format(
                            "Car dimensions: {0}x{1}",
                            game.Race.Cars[0].Dimensions.Length,
                            game.Race.Cars[0].Dimensions.Width
                        )
                    );
                    send(new Ping());
                    break;
                case "gameEnd":
                    Console.WriteLine("Race ended");
                    send(new Ping());
                    break;
                case "gameStart":
                    Console.WriteLine("Race starts");
                    send(new Ping());
                    break;
                case "yourCar":
                    carId = msg.DataAs<CarId>();
                    Console.WriteLine(String.Format("Your car name is '{0}' with color '{1}'",
                            carId.Name,
                            carId.Color));
                    break;
                case "crash":
                    var crashCarId = msg.DataAs<CarId>();
                    Console.WriteLine(String.Format("Car '{0}' crashed on tick {1}",
                            crashCarId.Name,
                            msg.GameTick));
                    break;
                case "spawn":
                    var spawnCarId = msg.DataAs<CarId>();
                    Console.WriteLine(String.Format("Car '{0}' has spawned on tick {1}",
                            spawnCarId.Name,
                            msg.GameTick));
                    break;
                default:
                    Console.WriteLine(msg.MsgType);
                    Console.WriteLine(line);
                    send(new Ping());
                    break;
            }
        }
    }

    private void send(SendMsg msg)
    {
        //Console.WriteLine(msg.ToJson());
        writer.WriteLine(msg.ToJson());
    }

	private void csvOutput (List<object> output)
	{
		for (int i=0; i<output.Count; i++) 
		{
			Console.Write(output[i]);
			Console.Write(i < output.Count-1 ? "," : ""); 
		}
		Console.WriteLine();
	}
}
