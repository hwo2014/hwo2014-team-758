using System;
using System.Collections.Generic;
using Protocol;

namespace Util
{
    public class Point
    {
        public double X { get; set; }

        public double Y { get; set; }

        public Point()
        {
        }

        public Point(Point p)
        {
            this.X = p.X;
            this.Y = p.X;
        }

        public Point(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }
    }

    public class Curve
    {
        public List<Point> Points { get; set; }

        public Curve(List<Point> points)
        {
            this.Points = points;
        }
    }

    public static class Distance
    {
        public static double Between(TrackInfo track, CarPosition pos1, CarPosition pos2)
        {
            if (pos1.PiecePosition.PieceIndex == pos2.PiecePosition.PieceIndex)
            {
                return pos2.PiecePosition.InPieceDistance - pos1.PiecePosition.InPieceDistance;
            }
            Lane lane = track.Lanes[(int)pos1.PiecePosition.Lane.EndLaneIndex];
            double part = ToPieceEnd(lane, track.Pieces[(int)pos1.PiecePosition.PieceIndex], pos1.PiecePosition.InPieceDistance);
            return part + pos2.PiecePosition.InPieceDistance;
        }

        private static double BezierPoint(double t, double start, double control_1, double control_2, double end)
        {
            /* Formula from Wikipedia article on Bezier curves. */
            return start * (1.0 - t) * (1.0 - t) * (1.0 - t)
            + 3.0 * control_1 * (1.0 - t) * (1.0 - t) * t
            + 3.0 * control_2 * (1.0 - t) * t * t + end * t * t * t;
        }

        public static double CubicBezierLength(Point start, Curve c, uint steps)
        {
            double t;
            var dot = new Point();
            var previousDot = new Point();
            double length = 0.0;

            for (int i = 0; i <= steps; i++)
            {
                t = (double)i / (double)steps;
                dot.X = BezierPoint(t, start.X, c.Points[0].X,
                    c.Points[1].X, c.Points[2].X);
                dot.Y = BezierPoint(t, start.Y, c.Points[0].Y,
                    c.Points[1].Y, c.Points[2].Y);
                if (i > 0)
                {
                    double xDiff = dot.X - previousDot.X;
                    double yDiff = dot.Y - previousDot.Y;
                    length += Math.Sqrt(xDiff * xDiff + yDiff * yDiff);
                }
                previousDot = new Point(dot);
            }
            return length;
        }

        private static double RadiusLengthInCurve(double r, double leftLaneDist, double rightLaneDist)
        {
            double avg = (Math.Abs(leftLaneDist) + Math.Abs(2)) / 2.0;
            if (leftLaneDist == rightLaneDist)
                return r;
            if ((Math.Sign(leftLaneDist) == Math.Sign(rightLaneDist)) && leftLaneDist < 0.0) // both lanes are on the left side
				return r + avg;
            if ((Math.Sign(leftLaneDist) == Math.Sign(rightLaneDist)) && leftLaneDist > 0.0) // both lanes are on the right side

			if (Math.Abs(leftLaneDist) > Math.Abs(rightLaneDist)) // left lane is further than right and the central line is between
					return r + Math.Abs(leftLaneDist) - avg;
            if (Math.Abs(rightLaneDist) > Math.Abs(leftLaneDist)) //right lane is further than left and the central line is between
				return r - (Math.Abs(rightLaneDist) - avg);
            return r;
        }

        public static double ToPieceEnd(Lane lane, TrackPiece piece, double inPieceDistance)
        {
            return piece.LengthForLane(lane) - inPieceDistance;
        }

        public static double ToPiece(TrackInfo track, CarPosition pos, uint pieceIndex, IDictionary<uint, SwitchDirection> switches)
        {
            Lane lane = track.Lanes[(int)pos.PiecePosition.Lane.EndLaneIndex];
            double distance = 0.0;
            for (uint i = pos.PiecePosition.PieceIndex; i != pieceIndex; i = (uint)((int)(i + 1) % track.Pieces.Count))
            {
                if (switches.ContainsKey(i))
                {
                    lane = track.Lanes[(int)lane.Index + (switches[i] == SwitchDirection.Left ? -1 : 1)];
                }
                distance += (i == pos.PiecePosition.PieceIndex) ?
                    ToPieceEnd(lane, track.Pieces[(int)i], pos.PiecePosition.InPieceDistance) :
                    track.Pieces[(int)i].LengthForLane(lane);
            }
            return distance;
        }
    }
}
