extern crate serialize;
extern crate collections;

use std::io::BufferedStream;
use std::io::net::addrinfo::get_host_addresses;
use std::io::net::ip::{IpAddr, SocketAddr};
use std::io::net::tcp::TcpStream;
use std::os::args;

use serialize::json;
use serialize::json::{Json, ToJson, List};
use collections::treemap::TreeMap;

trait Protocol {
    fn msg_type(&self) -> ~str;
    fn json_data(&self) -> Json;
}

struct Msg {
    msgType: ~str,
    data: Json
}

impl Protocol for Msg {
    fn msg_type(&self) -> ~str { self.msgType.clone() }
    fn json_data(&self) -> Json { self.data.clone() }
}

struct JoinMsg {
    name: ~str,
    key: ~str
}

impl Protocol for JoinMsg {
    fn msg_type(&self) -> ~str { ~"join" }
    fn json_data(&self) -> Json {
        let mut m = TreeMap::new();
        m.insert(~"name", self.name.to_json());
        m.insert(~"key", self.key.to_json());
        return json::Object(~m);
    }
}

struct ThrottleMsg {
    value: f64
}

impl Protocol for ThrottleMsg {
    fn msg_type(&self) -> ~str { ~"throttle" }
    fn json_data(&self) -> Json { json::Number(self.value) }
}

trait FromJSON {
    fn from_json(j: &Json) -> Option<Self>;
}

#[deriving(Show)]
enum PieceType {
    Straight(f64),
    Bend(int, f64),
}

impl FromJSON for PieceType {
    fn from_json(j: &Json) -> Option<PieceType> {
        match j.find(&~"length") {
            Some(l) => Some(Straight(l.as_number().unwrap())),
            None => {
                match (j.find(&~"radius"), j.find(&~"angle")) {
                    (Some(r), Some(a)) =>
                        Some(Bend(r.as_number().unwrap() as int,
                                  a.as_number().unwrap())),
                    _ => None
                }
            }
        }
    }
}

#[deriving(Show)]
struct Piece {
    pieceType: PieceType,
    switch: bool,
}

impl FromJSON for Piece {
    fn from_json(j: &Json) -> Option<Piece> {
        match FromJSON::from_json(j) {
            Some(pt) => {
                let switch = match j.find(&~"switch") {
                    Some(s) => s.as_boolean().unwrap(),
                    None => false
                };
                Some(Piece {
                    pieceType: pt,
                    switch: switch
                })
            },
            None => None
        }
    }
}

#[deriving(Show)]
struct Lane {
    distanceFromCenter: int,
    index: uint,
}

impl FromJSON for Lane {
    fn from_json(j: &Json) -> Option<Lane> {
        match (j.find(&~"distanceFromCenter"), j.find(&~"index")) {
            (Some(d), Some(i)) => Some(Lane {
                distanceFromCenter: d.as_number().unwrap() as int,
                index: i.as_number().unwrap() as uint
            }),
            _ => None
        }
    }
}

#[deriving(Show)]
struct StartingPoint {
    position: (f64, f64),
    angle: f64,
}

impl FromJSON for StartingPoint {
    fn from_json(j: &Json) -> Option<StartingPoint> {
        match (j.find(&~"angle"), j.find_path([&~"position", &~"x"]), j.find_path([&~"position", &~"y"])) {
            (Some(a), Some(x), Some(y)) => Some(StartingPoint {
                position: (x.as_number().unwrap(), y.as_number().unwrap()),
                angle: a.as_number().unwrap()
            }),
            _ => None
        }
    }
}

#[deriving(Show)]
struct Track {
    id: ~str,
    name: ~str,
    pieces: Vec<Piece>,
    lanes: Vec<Lane>,
    startingPoint: StartingPoint,
}

fn decode_list<T: FromJSON>(j: &Json) -> Option<Vec<T>> {
    let mut v = Vec::new();
    match j {
        &List(ref l) => {
            for e in l.iter() {
                match FromJSON::from_json(e) {
                    Some(de) => v.push(de),
                    None => return None
                }
            }
        }
        _ => return None
    }
    Some(v)
}

impl FromJSON for Track {
    fn from_json(j: &Json) -> Option<Track> {
        match (j.find(&~"id"), j.find(&~"name"), j.find(&~"pieces"), j.find(&~"lanes"), j.find(&~"startingPoint")) {
            (Some(id), Some(n), Some(p), Some(l), Some(s)) => {
                Some(Track {
                    id: id.as_string().unwrap().to_owned(),
                    name: n.as_string().unwrap().to_owned(),
                    pieces: decode_list(p).unwrap(),
                    lanes: decode_list(l).unwrap(),
                    startingPoint: FromJSON::from_json(s).unwrap()
                })
            }
            _ => None
        }
    }
}

#[deriving(Show)]
struct CarId {
    name: ~str,
    color: ~str,
}

impl FromJSON for CarId {
    fn from_json(j: &Json) -> Option<CarId> {
        match (j.find(&~"name"), j.find(&~"color")) {
            (Some(n), Some(c)) => Some(CarId {
                name: n.as_string().unwrap().to_owned(),
                color: c.as_string().unwrap().to_owned()
            }),
            _ => None
        }
    }
}

#[deriving(Show)]
struct Dimensions {
    length: f64,
    width: f64,
    guideFlagPosition: f64,
}

impl FromJSON for Dimensions {
    fn from_json(j: &Json) -> Option<Dimensions> {
        match (j.find(&~"length"), j.find(&~"width"), j.find(&~"guideFlagPosition")) {
            (Some(l), Some(w), Some(f)) => Some(Dimensions {
                length: l.as_number().unwrap(),
                width: w.as_number().unwrap(),
                guideFlagPosition: f.as_number().unwrap()
            }),
            _ => None
        }
    }
}

#[deriving(Show)]
struct Car {
    id: CarId,
    dimensions: Dimensions,
}

impl FromJSON for Car {
    fn from_json(j: &Json) -> Option<Car> {
        match (j.find(&~"id"), j.find(&~"dimensions")) {
            (Some(id), Some(d)) => Some(Car {
                id: FromJSON::from_json(id).unwrap(),
                dimensions: FromJSON::from_json(d).unwrap()
            }),
            _ => None
        }
    }
}

#[deriving(Show)]
struct RaceSession {
    laps: uint,
    maxLapTimeMs: uint,
    quickRace: bool,
}

impl FromJSON for RaceSession {
    fn from_json(j: &Json) -> Option<RaceSession> {
        match (j.find(&~"laps"), j.find(&~"maxLapTimeMs"), j.find(&~"quickRace")) {
            (Some(l), Some(mt), Some(qr)) => Some(RaceSession {
                laps: l.as_number().unwrap() as uint,
                maxLapTimeMs: mt.as_number().unwrap() as uint,
                quickRace: qr.as_boolean().unwrap()
            }),
            _ => None
        }
    }
}

#[deriving(Show)]
struct Race {
    track: Track,
    cars: Vec<Car>,
    raceSession: RaceSession,
}

impl FromJSON for Race {
    fn from_json(j: &Json) -> Option<Race> {
        match j.find(&~"race") {
            Some(r) => {
                match (r.find(&~"track"), r.find(&~"cars"), r.find(&~"raceSession")) {
                    (Some(t), Some(c), Some(rs)) => Some(Race {
                        track: FromJSON::from_json(t).unwrap(),
                        cars: decode_list(c).unwrap(),
                        raceSession: FromJSON::from_json(rs).unwrap()
                    }),
                    _ => None
                }
            }
            None => None
        }
    }
}

fn write_msg<T: Protocol>(msg: &T, stream: &mut BufferedStream<TcpStream>) {
    let mut json = TreeMap::new();
    json.insert(~"msgType", msg.msg_type().to_json());
    json.insert(~"data", msg.json_data());

    write_json(&json::Object(~json), stream);
}

fn write_json(json: &Json, stream: &mut BufferedStream<TcpStream>) {
    let s = json.to_str();
    let _ = stream.write_str(s.as_slice());
    let _ = stream.write_char('\n');
    let _ = stream.flush();
}

fn parse_msg(json: &~json::Object) -> Option<Msg> {
    match json.find(&~"msgType") {
        Some(&json::String(ref msgType)) => {
            let null = json::Null;
            let data = json.find(&~"data").unwrap_or(&null);
            Some(Msg {
                msgType: msgType.clone(),
                data: data.clone()
            })
        }
        _ => None
    }
}

struct RaceState {
    tick: uint
}
impl RaceState {
    fn handle_msg(&self, msg: ~Msg, stream: &mut BufferedStream<TcpStream>) {
        match msg.msgType.as_slice() {
            "carPositions" =>
            {
                let throttle = if self.tick == 0 {
                    0.5
                } else {
                    0.0
                };
                match msg.data {
                    List(ref l) => {
                        let first = l.head().unwrap();
                        println!("pieceIndex = {} inPieceDistance = {}", first.find_path([&~"piecePosition", &~"pieceIndex"]), first.find_path([&~"piecePosition", &~"inPieceDistance"]));
                    }
                    _ => ()
                }
                write_msg(&ThrottleMsg {
                    value: throttle
                }, stream)
            }
            _ => {
                match msg.msgType.as_slice() {
                    "join" => println!("Joined"),
                    "gameInit" => {
                        println!("Race init");
                        let init: Option<Race> = FromJSON::from_json(&msg.data);
                        println!("{}", init)
                    }
                    "raceEnd" => println!("Race end"),
                    "raceStart" => println!("Race start"),
                    _ => println!("Got {:s}", msg.msgType)
                }
                write_msg(&Msg {
                    msgType: ~"ping",
                    data: json::Null
                }, stream);
            }
        }
    }
}

fn start(config: Config) {
    let Config { server, name, key } = config;

    println!("Attempting to connect to {:s}", server.to_str());
    let mut stream = BufferedStream::new(TcpStream::connect(server).unwrap());
    println!("Connected");

    write_msg(&JoinMsg {
        name: name,
        key: key
    }, &mut stream);

    let gameState = RaceState {
        tick: 0
    };
    loop {
        match stream.read_line() {
            Err(_) => break,
            Ok(line) => match json::from_str(line) {
                Ok(json::Object(ref v)) => {
                    match parse_msg(v) {
                        None => println!("Invalid JSON data"),
                        Some(msg) => gameState.handle_msg(~msg, &mut stream)
                    }
                },
                Ok(_) => println!("Invalid JSON data: expected an object"),
                Err(msg) => println!("{}", msg.to_str())
            }
        }
    }
    println!("Disconnected")
}

struct Config {
    server: SocketAddr,
    name: ~str,
    key: ~str
}

fn resolve_first_ip(host: &str) -> Option<IpAddr> {
    match get_host_addresses(host) {
        Ok(ref addrs) => addrs.head().map(|x| x.clone()),
        _ => None
    }
}

fn read_config() -> Option<Config> {
    let args = args();
    match args.as_slice() {
        [_, ref host, ref port_str, ref name, ref key] => {
            let ip = resolve_first_ip(host.clone()).expect("Could not resolve host");
            let port = from_str::<u16>(port_str.clone()).expect("Invalid port number");
            return Some(Config {
                server: SocketAddr { ip: ip, port: port },
                name: name.clone(),
                key: key.clone()
            });
        },
        _ => None
    }
}

fn main() {
    match read_config() {
        None => println!("Usage: ./run <host> <port> <botname> <botkey>"),
        Some(config) => start(config)
    }
}
